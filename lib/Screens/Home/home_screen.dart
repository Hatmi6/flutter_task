import 'package:flutter/material.dart';
import 'package:my_first_app/Screens/Settings/settings_screen.dart';
  
   

  

class HomePage extends StatelessWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;




  @override
  Widget build(BuildContext context) {


return Scaffold(
      appBar: AppBar(title: Text(title)),
      body:   ListView(
                  padding: const EdgeInsets.all(8),
                  children: List.generate(choices.length, (index) {  
                    return Center(  
                      child: SelectCard(choice: choices[index] ),  
                    );  
                  }  
                  )        
             ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('LOGO'),
             ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Settings'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsPage()));
              },
            ),
          ],
        ),
      ),
    );
  }

}




class Choice {  
  const Choice({  
                  required this.number, 
                  required this.name, 
                  required this.num, 
                  required this.status, 
                  required this.icon, 
              });  
              
   final String number;  
  final String num;  
  final String status;  
  final String name;  
   final IconData icon;
  
 
 }  
  
const List<Choice> choices = const <Choice>[  
  const Choice(icon: Icons.note_add_outlined, number : '2021074848484848' , name : "Green house for trading 1" , num : '3/0', status : 'pending' ), 
  const Choice(icon: Icons.note_add_outlined, number : '2021054993837282' , name : "Green house for trading 2" , num : '2/0', status : 'pending' ), 
  const Choice(icon: Icons.note_add_outlined, number : '2021074848344221' , name : "Green house for trading 3" , num : '5/0', status : 'pending' ), 
  const Choice(icon: Icons.note_add_outlined, number : '2021077777534342' , name : "Green house for trading 4" , num : '4/0', status : 'pending' ), 
  const Choice(icon: Icons.note_add_outlined, number : '2021077676544533' , name : "Green house for trading 5" , num : '7/0', status : 'pending' ), 
  const Choice(icon: Icons.note_add_outlined, number : '2021074876767666' , name : "Green house for trading 6" , num : '8/0', status : 'pending' ), 
];  
  
class SelectCard extends StatelessWidget {  
  const SelectCard({required this.choice});  
  final Choice choice;  
  
  @override  
  Widget build(BuildContext context) {  
     return Container(
        height: 150,
        //decoration: (),
      child: Card(  
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: 10,
          shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white70, width: 1),
                    borderRadius: BorderRadius.only(
                                                      topLeft: Radius.circular(0), 
                                                      topRight: Radius.circular(20), 
                                                      bottomRight: Radius.circular(20),
                                                      bottomLeft: Radius.circular(20),
                                                      ),
                  
                ),
        child: Row(  
                mainAxisSize: MainAxisSize.max,
                
                children: [  
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                    Icon(choice.icon, size:40), 
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                      Row(
                        children: <Widget>[
                        Text("Order number: " ,   textAlign: TextAlign.start, style: TextStyle(fontWeight: FontWeight.bold)),            
                        Text(choice.number),            
                      ],),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                        Text("Name: " , textAlign: TextAlign.start, style: TextStyle( fontWeight: FontWeight.bold)),            
                        Text(choice.name),            
                      ],),
                      Row(children: <Widget>[
                        Text("Required number: " ,    textAlign: TextAlign.start, style: TextStyle(fontWeight: FontWeight.bold)),            
                        Text(choice.num),            
                      ],),
                      Row(
                          children: <Widget>[
                         Text(choice.status ,   textAlign: TextAlign.start, style: TextStyle(fontWeight: FontWeight.bold)),            
                      ],),
                      
                   ],
                  mainAxisSize: MainAxisSize.min,
                    )
                  ],
                  )
            ]  
        ),  
       ) 
    );  

  }  
}