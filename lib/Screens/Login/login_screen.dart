import 'dart:convert';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;
import 'package:my_first_app/Screens/Home/home_screen.dart';


class LoginPage extends StatefulWidget {
  

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
 
  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
            Text('Back',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

 
  Widget _submitButton() {
    return Container(
      
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 15),
      alignment: Alignment.center,
      
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.shade200,
                offset: Offset(2, 4),
                blurRadius: 5,
                spreadRadius: 2)
          ],
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Color(0xfffbb448), Color(0xfff7892b)])
        ),
        
      child: InkWell(
          onTap: () async {

            if(USERNAME.text != "" && PASSWORD.text != ""){

    var headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cookie': 'CFID=1075314; CFTOKEN=fd56b20f01fb1a3c-9CD7E287-E785-21CC-DE5B5B6AF59E269E; JSESSIONID=E11F8A190FE627EA1CF790B4306F6730.cfusion'
    };
 
 try{

  var response = await http.post(
    Uri.parse('http://82.212.90.190:7777/rest/OmanAPI_DEV/MobileAPI'),
    headers: headers,
    body: {
        'args': '{"ENTITY":"CONFIG","ACTION":"LOGIN",'+
        '"DATA":{"USER":{"SESSION":"2","URLTOKEN":"","USER_ID":"","USER_SOURCE":"EXTERNAL","LANGUAGE":1},'+
        '"data":{"APPLICATION_NAME":"OMANTCS","LANGUAGE":"1",'+
        '"PASSWORD":'+PASSWORD.text +','+
        '"REMEMBER_ME":"0",'+
        '"USERNAME":'+USERNAME.text +','+
        '"USER_DEVICE_INFO":{"APPLICATION_VERSION":"2.0.0","DEVICE_ID":"649F60AC-C952-4DFF-B518-1E75DFBDF6B5","DEVICE_IN_INCH":"test","DEVICE_MAC_ADDRESS":"GetMac.macAddress","DEVICE_MANUFACTURE":"APPLE","DEVICE_MODEL":"iPhone","DEVICE_OS":"Darwin","DEVICE_OS_VERSION":"12.5.4","FCM_TOKEN":"7a667cfd-ae2c-4c7c-9aca-636eb6b0b934"}}}}'
    },
  );
 
  if (response.statusCode == 201) {
    
    print(jsonDecode(response.body) );
  } else { 
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog( 
          content: Text("Login Failed."),
        );
      },
    );
    throw Exception('Login Failed.');
  }
 }catch(e){

   showDialog(
      context: context,
      builder: (context) {
        return AlertDialog( 
          content: Text("Error in api"),
        );
      },
    );
 
 print("ERROR");
 print(e);


 


 }
           


            }else{
              showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog( 
                        content: Text("Please Fill this fields"),
                      );
                    },
                  );
            }


             
          },
          child: new Padding(
            padding: new EdgeInsets.all(10.0),
            child: new Text("Sign in"),
          ),
    ),

    );
  }
 

 

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
                text: 'TASK',
               style: TextStyle(color: Colors.black, fontSize: 30),
      ),
    );
  }


 
  // ignore: non_constant_identifier_names
  final USERNAME = TextEditingController();
  // ignore: non_constant_identifier_names
  final PASSWORD = TextEditingController();


  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Container(
      height: height,
      child: Stack(
        children: <Widget>[
      
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: height * .2),
                  _title(),
                  SizedBox(height: 50),
                  Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: TextField(
                             controller: USERNAME,
                             decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'User name'
                            ),
                          ),
                        ),
                  Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: TextField(
                            obscureText: true,
                            controller: PASSWORD,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Password'
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                  _submitButton(),
                  InkWell(
                             onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(title: 'Home',)));
                                },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(vertical: 13),
                            alignment: Alignment.center,
                           
                            child: Text(
                              'Skip',
                              style: TextStyle(fontSize: 10, color: Color(0xfff7892b)),
                            ),
                          ),
                        )

                ],
              ),
            ),
          ),
                  Positioned(top: 40, left: 0, child: _backButton()),

         ],

      ),
    ));
  }
}