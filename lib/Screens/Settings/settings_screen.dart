
import 'package:flutter/material.dart';
 import 'package:my_first_app/Screens/Home/home_screen.dart';
import 'package:my_first_app/Screens/Login/login_screen.dart';


class SettingsPage extends StatefulWidget {  
  @override  
  SettingsPage_ createState() => new SettingsPage_();  
}  
  

// ignore: camel_case_types
class SettingsPage_ extends State {
 
 
   bool isSwitched = false;  

    List<bool> _isSelected = [false, true];


  var textValue = 'Notification disabled';  
  
  void toggleSwitch(bool value) {  
  
    if(isSwitched == false)  
    {  
       setState(() {  
          isSwitched = true;
          textValue = 'Notification enabled';  
      });

      print('Switch Button is ON'); 
      
       
    }  
    else  
    {   
      setState(() {  
          isSwitched = false;
          textValue = 'Notification disapled';  
      });
      print('Switch Button is OFF');  
    }  
  }  


  @override
  Widget build(BuildContext context) {




return Scaffold(
      appBar: AppBar(title: Text("Settings")),
      body: Column(children: [
          

 Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.notifications),
              title: Text('Enable and disable notification:'),
             ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                  Switch(  
                        onChanged: toggleSwitch,  
                        value: isSwitched,  
                      ),
                  Text('$textValue', style: TextStyle(fontSize: 12),) 
                  ],
            ),
          ],
        ),
      ),
   

 Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.language),
              title: Text('Change Language:'),
             ),
              ToggleButtons(
                      children: <Widget>[
                        Text("Arabic",  style: TextStyle(fontSize: 15,)),
                        Text("English",  style: TextStyle(fontSize: 15,)),
                       ],
                      
                      isSelected: _isSelected,
                      onPressed: (int index) {
                        setState(() {
                            for (int buttonIndex = 0; buttonIndex < _isSelected.length; buttonIndex++) {
                              if (buttonIndex == index) {
                                _isSelected[buttonIndex] = !_isSelected[buttonIndex];
                              } else {
                                _isSelected[buttonIndex] = false;
                              }
                            }
                          });
                      },
                    ),
           ],
        ),
      ),
   


 Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.logout),
              title: Text('Logout:'),
             ),
                   InkWell(
                             onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
                                },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(vertical: 13),
                            alignment: Alignment.center,
                           
                            child: Text(
                              'Logout',
                              
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        )
           ],
        ),
      ),
   

     
 


      ],),

      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text('LOGO'),
             ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(title: "Home")));

              },
            ),
            ListTile(
              title: const Text('Settings'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                  Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }

}



